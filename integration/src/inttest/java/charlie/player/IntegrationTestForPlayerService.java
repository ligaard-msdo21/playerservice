package charlie.player;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;;

import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
public class IntegrationTestForPlayerService {

    @Container
    public GenericContainer redisContainer =
            new GenericContainer("redis:6.2-alpine")
                    .withExposedPorts(6379);

    @BeforeEach
    public void setUp() throws Exception {
    }

    @Tag("Fails")
    @Test
    public void redisContainerIsAvailableA() {
        assertNotNull(redisContainer.getFirstMappedPort());
    }

    @Tag("Works")
    @Test
    public void redisContainerIsAvailableB() {
        assertEquals(redisContainer.getContainerIpAddress(), "localhost");
    }
}