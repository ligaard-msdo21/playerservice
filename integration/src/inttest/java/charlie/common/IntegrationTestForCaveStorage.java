package charlie.common;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
public class IntegrationTestForCaveStorage {
    protected CaveStorage storage;
    private SubscriptionRecord sub1;

    // TODO: Uses Redis cave storage convention for player id.
    private String subscriptionId1 = "player-1";

    @Container
    public GenericContainer redisContainer =
            new GenericContainer("redis:6.2.6-alpine")
                    .withExposedPorts(6379);

    @BeforeEach
    public void setUp() throws Exception {
        storage = new RedisCaveStorageConnector();
        storage.initialize(null, new ServerConfiguration(
                redisContainer.getContainerIpAddress(),
                redisContainer.getFirstMappedPort()));
        sub1 = new SubscriptionRecord(subscriptionId1,"Tutmosis", "grp01", Region.ODENSE);
    }

    private String p000 = "(0,0,0)";
    private String p273 = "(2,7,3)";

    @Tag("Works")
    @Test
    public void shouldUpdateRoomTables() {
        RoomRecord room;

        // validate that rooms can be created
        int canAdd = storage.addRoom(p273, new RoomRecord("You are in a dark lecturing hall.", CaveStorage.WILL_CROWTHER_ID));
        assertEquals(HttpServletResponse.SC_CREATED, canAdd);

        room = storage.getRoom(p273);
        assertEquals("You are in a dark lecturing hall.", room.getDescription());
        assertEquals(CaveStorage.WILL_CROWTHER_ID, room.getCreatorId());

        // validate that existing rooms cannot be overridden
        canAdd = storage.addRoom(p273, new RoomRecord("This room must never be made", "some-id"));

        assertEquals(HttpServletResponse.SC_FORBIDDEN, canAdd);

        int canUpdateIfSameUser = storage.updateRoom(p273, new RoomRecord("Updated room", CaveStorage.WILL_CROWTHER_ID));
        assertEquals(HttpServletResponse.SC_OK, canUpdateIfSameUser);

        int cannotUpdateIfNotRoomCreator = storage.updateRoom(p273, new RoomRecord("Update by non-creator", "not-creator-id"));
        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, cannotUpdateIfNotRoomCreator);

        int cannotUpdateNonExistingRoom = storage.updateRoom("room(90,90,90", new RoomRecord("Update a room that does not exist", CaveStorage.WILL_CROWTHER_ID));
        assertEquals(HttpServletResponse.SC_NOT_FOUND, cannotUpdateNonExistingRoom);
    }

    @Test
    public void validateRoomExits() {
        String p333 = "(3,3,3)"; // "origo" room
        String p433 = "(4,3,3)"; // East room
        String p343 = "(3,4,3)"; // North room
        String p334 = "(3,3,4)"; // Up room
        String p233 = "(2,3,3)"; // West room
        String p323 = "(3,2,3)"; // South room
        String p332 = "(3,3,2)"; // Down room
        storage.addRoom(p433, new RoomRecord("East room", CaveStorage.WILL_CROWTHER_ID));
        storage.addRoom(p343, new RoomRecord("North room", CaveStorage.WILL_CROWTHER_ID));
        List<Direction> exits = storage.getSetOfExitsFromRoom(p333);

        assertTrue(exits.contains(Direction.EAST));
        assertFalse(exits.contains(Direction.WEST));
        assertTrue(exits.contains(Direction.NORTH));
        assertFalse(exits.contains(Direction.SOUTH));
        assertFalse(exits.contains(Direction.DOWN));
        assertFalse(exits.contains(Direction.UP));

    }

    private void addPlayerRecordToStorageForSubscription(SubscriptionRecord sub01) {
        PlayerRecord rec1 = new PlayerRecord(sub01, p000);
        storage.updatePlayerRecord(rec1);
    }

    @Test
    public void validatePlayerFetching() {
        addPlayerRecordToStorageForSubscription(sub1);
        PlayerRecord player1 = storage.getPlayerByID(sub1.getPlayerID());
        assertTrue(player1 instanceof PlayerRecord);
        assertEquals("Tutmosis", player1.getPlayerName());
        assertEquals("grp01", player1.getGroupName());
        assertEquals(Region.ODENSE, player1.getRegion());
    }

    @Test
    public void validatePlayersInRoom() {
        addPlayerRecordToStorageForSubscription(sub1);
        PlayerRecord player1 = storage.getPlayerByID(sub1.getPlayerID());
        player1.setPositionAsString(p000);
        storage.updatePlayerRecord(player1);

        List<PlayerRecord> players = storage.computeListOfPlayersAt(p000);
        assertTrue(players.get(0) instanceof PlayerRecord);
        assertEquals("Tutmosis", players.get(0).getPlayerName());
    }

    @Test
    public void shouldHaveInitialFourRooms() {
        RoomRecord room = storage.getRoom(p000);
        assertEquals("You are standing at the end of a road before a small brick building.", room.getDescription());
    }
}
