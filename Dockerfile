# Build for multi-architecture (https://www.docker.com/blog/multi-arch-build-and-images-the-simple-way/):
#   $ docker buildx create --use # Only needed first time, see above URL for requirements.
#   $ docker buildx build --push --platform linux/arm64/v8,linux/amd64 -t [yourimagename] -f Dockerfile .
#
# Usage:
#   $ docker run -d -p 7777:7777 --name myplayer [yourimagename] gradle player -Pcpf=[cpf file name]
#   $ docker run -d -p 7777:7777 --name myplayer [yourimagename]
#   $ docker run -ti --rm [yourimagename] gradle test jacocoRootReport
#   $ docker run -ti --net container:myplayer [yourimagename] gradle cmd -Pcpf=[cpf file name] -Pid=[loginName] -Ppwd=[password]

FROM ligaard/jdk17-gradle73:latest

LABEL maintainer="KasperLigaard_msdo@ligaard.dk"

RUN apt-get update && \
    apt-get install -y --no-install-recommends curl # for health check

WORKDIR /root/player
COPY gradle/ gradle/
COPY integration/ integration/
COPY player/ player/
COPY build.gradle \
    Dockerfile \
    gradle.properties \
    gradlew \
    settings.gradle ./

HEALTHCHECK --interval=30s --timeout=1s --start-period=1m \
    CMD curl -f http://localhost:7778/skycave/health || exit 1

EXPOSE 7778:7778

CMD ["gradle", "player", "-Pcpf=http.cpf"]
