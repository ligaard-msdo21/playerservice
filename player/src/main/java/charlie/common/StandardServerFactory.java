/*
 * Copyright (c) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package charlie.common;

import com.baerbak.cpf.PropertyReaderStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;

/**
 * Concrete ServerFactory that creates server side delegates based upon dynamic
 * class loading of classes whose qualified names are defined by a set of
 * properties. After creation, each service delegate is configured through their
 * 'initialize' method with their service end point configuration, again based
 * upon reading their respective properties.
 * 
 * @see Config
 * 
 * @author Henrik Baerbak Christensen, University of Aarhus
 * 
 */
public class StandardServerFactory implements CaveServerFactory {

  private Logger logger;
  private PropertyReaderStrategy propertyReader;

  /**
   * Construct a new server factory, which creates delegates
   * by reading properties from the given reader strategy.
   * 
   * @param envReader
   *          the reader strategy for setting properties
   */
  public StandardServerFactory(PropertyReaderStrategy envReader) {
    logger = LoggerFactory.getLogger(StandardServerFactory.class);
    this.propertyReader = envReader;
  }

  @Override
  public CaveStorage createCaveStorageConnector(ObjectManager objMgr) {
    CaveStorage caveStorage = null;
    caveStorage = (CaveStorage)
            createServiceConnector(CaveStorage.class, Config.PLAYER_CAVESTORAGE, objMgr);
    return caveStorage;
  }

  @Override
  public ExternalService createServiceConnector(Type interfaceType, String propertyKeyPrefix, ObjectManager objMgr) {
    ExternalService serviceConnector = null;
    serviceConnector = Config.loadAndInstantiate(propertyReader,
            propertyKeyPrefix +Config.CONNECTOR_SUFFIX, serviceConnector);

    // Read in the serviceConnector configuration
    ServerConfiguration config =
            new ServerConfiguration(propertyReader, propertyKeyPrefix +Config.SERVER_ADDRESS_SUFFIX);
    serviceConnector.initialize(objMgr, config);

    logger.info("method=createServiceConnector, type="
            + interfaceType.getTypeName()
            + ", connectorImplementation="
            + serviceConnector.getClass().getName()
            + ", serverAddress=" + config);

    return serviceConnector;
  }

}
