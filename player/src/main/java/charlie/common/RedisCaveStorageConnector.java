package charlie.common;

import com.google.gson.Gson;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.HttpServletResponse;
import java.time.ZonedDateTime;
import java.util.*;

public class RedisCaveStorageConnector implements CaveStorage {
    private JedisPool jedisPool;
    private final Gson gson;

    private ServerConfiguration configuration;

    public RedisCaveStorageConnector() {
        gson = new Gson();
    }

    @Override
    public RoomRecord getRoom(String positionString) {
        try (Jedis jedis = jedisPool.getResource()) {
            String asJSON = jedis.get("room" + positionString);
            if (asJSON == null) return null;
            return gson.fromJson(asJSON, RoomRecord.class);
        }
    }

    @Override
    public int addRoom(String positionString, RoomRecord roomRecord) {
        try (Jedis jedis = jedisPool.getResource()) {
            String roomId = "room" + positionString;
            String roomExists = jedis.get(roomId);
            if (roomExists == null) {
                roomRecord.setId(roomId);
                roomRecord.setCreationTime(ZonedDateTime.now());
                String asJSON = gson.toJson(roomRecord);
                jedis.set(roomId, asJSON);
                return HttpServletResponse.SC_CREATED;
            }
            return HttpServletResponse.SC_FORBIDDEN;
        }
    }

    @Override
    public int updateRoom(String positionString, RoomRecord updatedRoom) {
        try (Jedis jedis = jedisPool.getResource()) {
            String asJSON = jedis.get("room" + positionString);
            if (asJSON != null) { // Room exists
                RoomRecord existingRoom = gson.fromJson(asJSON, RoomRecord.class);
                if (existingRoom.getCreatorId().equals(updatedRoom.getCreatorId())) {
                    String updatedAsJson = gson.toJson(updatedRoom);
                    jedis.set("room" + positionString, updatedAsJson);
                    return HttpServletResponse.SC_OK; // TODO: Assumes jedis.set() call succeeds. How to detect failing jedis.set()?
                } else {
                    return HttpServletResponse.SC_UNAUTHORIZED; // Not creator trying to update room.
                }
            }
            return HttpServletResponse.SC_NOT_FOUND; // NOT FOUND
        }
    }

    @Override
    public List<Direction> getSetOfExitsFromRoom(String positionString) {
        // TODO: Split in to x, y, z components and fetch all location on the four sides, top, and bottom.
        String changed = positionString.replace("(", "").replace(")","").replace(","," ");
        String[] tokens = changed.split("\\s");
        int x = Integer.parseInt(tokens[0]);
        int y = Integer.parseInt(tokens[1]);
        int z = Integer.parseInt(tokens[2]);
        List<String> adjecentLocations = Arrays.asList(
                (x+1) + "," + y + "," + z,
                x + "," + (y+1) + "," + z,
                x + "," + y + "," + (z+1),
                (x-1) + "," + y + "," + z,
                x + "," + (y-1) + "," + z,
                x + "," + y + "," + (z-1)
        );
        List<String> adjecentPositionStrings = Arrays.asList(
                "room(" + adjecentLocations.get(0) + ")",
                "room(" + adjecentLocations.get(1) + ")",
                "room(" + adjecentLocations.get(2) + ")",
                "room(" + adjecentLocations.get(3) + ")",
                "room(" + adjecentLocations.get(4) + ")",
                "room(" + adjecentLocations.get(5) + ")"
        );
        try (Jedis jedis = jedisPool.getResource()) {
            List<String> exits = jedis.mget(
                    adjecentPositionStrings.get(0),
                    adjecentPositionStrings.get(1),
                    adjecentPositionStrings.get(2),
                    adjecentPositionStrings.get(3),
                    adjecentPositionStrings.get(4),
                    adjecentPositionStrings.get(5)
            );

            List<Direction> directions = new ArrayList<Direction>();

            if (exits.get(0) != null) {
                directions.add(Direction.EAST);
            }
            if (exits.get(1) != null) {
                directions.add(Direction.NORTH);
            }
            if (exits.get(2) != null) {
                directions.add(Direction.UP);
            }
            if (exits.get(3) != null) {
                directions.add(Direction.WEST);
            }
            if (exits.get(4) != null) {
                directions.add(Direction.SOUTH);
            }
            if (exits.get(5) != null) {
                directions.add(Direction.DOWN);
            }
            return directions;
        }

    }

    @Override
    public PlayerRecord getPlayerByID(String playerID) {
        try (Jedis jedis = jedisPool.getResource()) {
            String asJSON = jedis.get("player-" + playerID);
            return gson.fromJson(asJSON, PlayerRecord.class);
        }
    }

    @Override
    public void updatePlayerRecord(PlayerRecord record) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set("player-" + record.getPlayerID(), gson.toJson(record));
        }
    }

    @Override
    public List<PlayerRecord> computeListOfPlayersAt(String positionString) {
        try (Jedis jedis = jedisPool.getResource()) {
            // TODO: Centralise player prefix somewhere, to avoid having to update here
            //  and in e.g. CaveregSubscriptionService.java
            String playerPrefix = "player-";
            // TODO: Poor scalability fetching all players in cave.
            Set<String> playerSet = jedis.keys(playerPrefix + "*");
            List<String> playerList = new ArrayList<>(playerSet);
            List<PlayerRecord> playersPresent = new ArrayList<PlayerRecord>();
            if (playerList.isEmpty()) {
                return playersPresent;
            }
            List<String> playersJSON = jedis.mget(playerList.stream().toArray(String[]::new));
            for (String asJSON : playersJSON) {
                PlayerRecord player = gson.fromJson(asJSON, PlayerRecord.class);
                if (player.isInCave() && player.getPositionAsString().equals(positionString)) {
                    playersPresent.add(player);
                }
            }
            return playersPresent;
        }
    }


    @Override
    public void addMessage(String positionInCave, MessageRecord messageRecord) {
        try (Jedis jedis = jedisPool.getResource()) {
            // TODO: Centralise 'wall' prefix to position string
            String asJSON = jedis.get("wall" + positionInCave);
            LinkedList<String> listOfJSON;
            if (asJSON == null) {
                listOfJSON = new LinkedList<String>();
            } else {
                listOfJSON = gson.fromJson(asJSON, LinkedList.class);
            }
            messageRecord.setCreatorTimeStampISO8601(ZonedDateTime.now());
            messageRecord.setId(UUID.randomUUID().toString());
            listOfJSON.addFirst(gson.toJson(messageRecord));
            jedis.set("wall" + positionInCave, gson.toJson(listOfJSON));
        }
    }

    @Override
    public int updateMessage(String positionInCave, String messageId, MessageRecord newMessageRecord) {
        // Update...
        try (Jedis jedis = jedisPool.getResource()) {
            String asJSON = jedis.get("wall" + positionInCave);
            LinkedList<String> listOfJSON = gson.fromJson(asJSON, LinkedList.class);
            List<String> pageRecords = new ArrayList<>();
            boolean matchFound = false;
            int returnValue = HttpServletResponse.SC_NOT_FOUND;
            for (String JSONMessageRecord : listOfJSON) {
                MessageRecord messageRecord = gson.fromJson(JSONMessageRecord, MessageRecord.class);
                if (!matchFound && messageRecord.getId().equals(messageId)) {
                    matchFound = true;
                    if (messageRecord.getCreatorId().equals(newMessageRecord.getCreatorId())) {
                        messageRecord.setContents(newMessageRecord.getContents());
                        returnValue = HttpServletResponse.SC_OK;
                    } else {
                        returnValue = HttpServletResponse.SC_UNAUTHORIZED;
                    }
                }
                pageRecords.add(gson.toJson(messageRecord));
            }
            jedis.set("wall" + positionInCave, gson.toJson(pageRecords));
            return returnValue;
        }
    }

    @Override
    public List<MessageRecord> getMessageList(String positionInCave, int startIndex, int pageSize) {
        try (Jedis jedis = jedisPool.getResource()) {
            String asJSON = jedis.get("wall" + positionInCave);
            ArrayList<String> listOfJSON = gson.fromJson(asJSON, ArrayList.class);
            List<MessageRecord> pageRecords = new ArrayList<>();
            if (listOfJSON == null || listOfJSON.isEmpty()) {
                return pageRecords;
            }

            // Handle list indexes potentially out of range.
            if (startIndex < 0) {
                startIndex = 0;
            } else if (startIndex >= listOfJSON.size()) {
                return pageRecords; // no records, return empty list
            }

            int endIndex = startIndex + pageSize;
            if (endIndex < startIndex) {
                endIndex = startIndex;
            } else if (endIndex > listOfJSON.size()) {
                endIndex = listOfJSON.size();
            }

            List<String> pagePostings = listOfJSON.subList(startIndex, endIndex);
            for (String posting : pagePostings) {
                pageRecords.add(gson.fromJson(posting, MessageRecord.class));
            }
            return pageRecords;
        }
    }


    @Override
    public ServerConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public void initialize(ObjectManager objMgr, ServerConfiguration config) {
        this.configuration = config;
        ServerData serverData = config.get(0);

        jedisPool = new JedisPool(serverData.getHostName(), serverData.getPortNumber());

        try (Jedis jedis = jedisPool.getResource()) {
            String asJSON = jedis.get("room"+"(0,0,0)");
            if (asJSON == null) {
                // Initialize the default room layout
                RoomRecord entryRoom = new RoomRecord(
                        "You are standing at the end of a road before a small brick building.",
                        WILL_CROWTHER_ID);
                addRoom(new Point3(0, 0, 0).getPositionString(), entryRoom);
                addRoom(new Point3(0, 1, 0).getPositionString(), new RoomRecord(
                        "You are in open forest, with a deep valley to one side.", WILL_CROWTHER_ID));
                addRoom(new Point3(1, 0, 0).getPositionString(), new RoomRecord(
                        "You are inside a building, a well house for a large spring.", WILL_CROWTHER_ID));
                addRoom(new Point3(-1, 0, 0).getPositionString(), new RoomRecord(
                        "You have walked up a hill, still in the forest.", WILL_CROWTHER_ID));
                addRoom(new Point3(0, 0, 1).getPositionString(), new RoomRecord(
                        "You are in the top of a tall tree, at the end of a road.", WILL_CROWTHER_ID));

            }
        }

    }

    @Override
    public void disconnect() {
        // Do nothing, for now.
    }
}
