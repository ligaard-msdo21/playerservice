/*
 * Copyright (C) 2021. Simon Møller and Kasper Ligaard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package charlie.player;

import charlie.common.*;
import com.baerbak.cpf.ChainedPropertyResourceFileReaderStrategy;
import com.baerbak.cpf.PropertyReaderStrategy;
import com.google.gson.Gson;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import static spark.Spark.*;

/**
 * @author Simon Møller & Kasper Ligaard
 */
public class Main {
  private static Logger logger = LoggerFactory.getLogger("Main");
  private static CaveStorage storage;
  private static Gson gson;

  public static void main(String[] args) {
    gson = new Gson();

    String cpfFileName = Config.prependDefaultFolderForNonPathFilenames(args[0]);

    // Create the abstract factory to create delegates using dependency injection.
    // The daemon always uses CPF files for defining delegates.
    PropertyReaderStrategy propertyReader = new ChainedPropertyResourceFileReaderStrategy(cpfFileName);
    CaveServerFactory factory = new StandardServerFactory(propertyReader);

    // Create the object manager that creates and holds all delegate references
    // for global access - a sort of lookup service/DNS/yellow pages for delegates
    ObjectManager objectManager = new StandardObjectManager(factory);
    ServerConfiguration storageConfiguration =
            new ServerConfiguration(propertyReader, Config.PLAYER_CAVESTORAGE + Config.SERVER_ADDRESS_SUFFIX);

    storage = objectManager.getCaveStorage();
    storage.initialize(objectManager, storageConfiguration);

    ServerConfiguration serverConfiguration =
            new ServerConfiguration(propertyReader, Config.PLAYER_APPSERVER);
    port(serverConfiguration.get(0).getPortNumber());

    path("/skycave/v1", () -> {
      get("/player/:playerID", Main::playerIdHandler);
      get("/players/:positionString", Main::playersAtHandler);
      post("/player/:id", Main::updatePlayerHandler);
      post("/player/", Main::updatePlayerHandler);
    });

    path("/skycave/v2", () -> {
      get("/player/:playerID", Main::correlationIDPlayerHandler);
      get("/players/:positionString", Main::playersAtHandler);
      post("/player/:id", Main::updatePlayerHandler);
      post("/player/", Main::updatePlayerHandler);
    });

    path("/skycave", () ->{
      get("/health", Main::serviceHealth);
    });
  }

  private static String serviceHealth(Request request, Response response) {
    response.status(HttpServletResponse.SC_OK);
    response.type( "application/json");
    return "{ \"state\": \"healthy\", \"title\": \"PlayerService by Simon M. Møller and Kasper Ligaard\", \"version\": \"v1\" }";
  }

  private static String correlationIDPlayerHandler(Request request, Response response) {
    String playerID = request.params(":playerID");
    String correlationID = request.queryParams(":correlationID");
    logger.info("method=" + request.requestMethod() + ", playerID=" + playerID + ", correlationID=" + correlationID);
    PlayerRecord player = storage.getPlayerByID(playerID);
    String responseJSON = gson.toJson(player);
    if (player == null) {
      response.status(HttpServletResponse.SC_NOT_FOUND);
      responseJSON = "{}";
    }
    response.type( "application/json");
    return responseJSON;
  }

  private static String playerIdHandler (Request request, Response response) {
    String responseJSON = correlationIDPlayerHandler(request, response);
    return responseJSON;
  }

  private static String playersAtHandler (Request request, Response response) {
    String positionString = request.params(":positionString");
    logger.info("method=" + request.requestMethod() + ", param=" + positionString);

    // REGEX that matches on a position string
    String REGEX = "\\(-?[0-9]{1,9},-?[0-9]{1,9},-?[0-9]{1,9}\\)"; // A bit lax, since 01 is allowed by regex.
    if (!Pattern.matches(REGEX, positionString)) {
      response.status(HttpServletResponse.SC_BAD_REQUEST);
      response.type( "application/json");
      return "[]";
    }

    List<PlayerRecord> players = storage.computeListOfPlayersAt(positionString);
    String listOfPlayersJSON = gson.toJson(players);
    response.type( "application/json");
    return listOfPlayersJSON;
  }

  private static String updatePlayerHandler (Request request, Response response) {
    String id = request.params(":id");
    logger.info("method=" + request.requestMethod() + ", param=" + id);
    String body = request.body();
    PlayerRecord playerRecord = gson.fromJson(body, PlayerRecord.class);

    if (id == null) {
      id = UUID.randomUUID().toString();
      // TODO: Access token will be 'void' in playerRecord (Probably OK as login will set it).
      SubscriptionRecord subscription = new SubscriptionRecord(
              id,
              playerRecord.getPlayerName(),
              playerRecord.getGroupName(),
              playerRecord.getRegion()
      );
      PlayerRecord newPlayerRecord = new PlayerRecord(subscription, playerRecord.getPositionAsString());
      playerRecord = newPlayerRecord;
      storage.updatePlayerRecord(playerRecord);
      response.status(HttpServletResponse.SC_CREATED);
    } else {
      response.status(HttpServletResponse.SC_OK);
      if (storage.getPlayerByID(id) == null) {
        response.status(HttpServletResponse.SC_CREATED);
      }
      storage.updatePlayerRecord(playerRecord);
    }

    response.header("Location", request.url() + "/" + id);
    response.type( "application/json");
    String responseJSON = gson.toJson(playerRecord);
    return responseJSON;
  }
}
