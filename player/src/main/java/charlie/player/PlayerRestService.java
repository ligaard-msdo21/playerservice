package charlie.player;

import charlie.common.PlayerRecord;
import com.google.gson.Gson;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PlayerRestService {
    private JedisPool jedisPool;
    private final Gson gson;

    public PlayerRestService() {
        gson = new Gson();
    }

    public PlayerRecord getPlayerByID(String playerID) {
        try (Jedis jedis = jedisPool.getResource()) {
            String asJSON = jedis.get("player-" + playerID);
            return gson.fromJson(asJSON, PlayerRecord.class);
        }
    }

    public void updatePlayerRecord(PlayerRecord record) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set("player-" + record.getPlayerID(), gson.toJson(record));
        }
    }

    public List<PlayerRecord> computeListOfPlayersAt(String positionString) {
        try (Jedis jedis = jedisPool.getResource()) {
            // TODO: Centralise player prefix somewhere, to avoid having to update here
            //  and in e.g. CaveregSubscriptionService.java
            String playerPrefix = "player-";
            // TODO: Poor scalability fetching all players in cave.
            Set<String> playerSet = jedis.keys(playerPrefix + "*");
            List<String> playerList = new ArrayList<>(playerSet);
            List<PlayerRecord> playersPresent = new ArrayList<PlayerRecord>();
            if (playerList.isEmpty()) {
                return playersPresent;
            }
            List<String> playersJSON = jedis.mget(playerList.stream().toArray(String[]::new));
            for (String asJSON : playersJSON) {
                PlayerRecord player = gson.fromJson(asJSON, PlayerRecord.class);
                if (player.isInCave() && player.getPositionAsString().equals(positionString)) {
                    playersPresent.add(player);
                }
            }
            return playersPresent;
        }
    }
}
