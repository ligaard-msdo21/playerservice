# PlayerService API v1.2

API discussion in Brightspace forum: https://brightspace.au.dk/d2l/le/30693/discussions/topics/30124/View (aka. *Overgruppe 1*, Alfa, Bravo and Charlie).

The project is setup to use JAVA 17, Gradle 7.3, and JUnit 5.8.

## Docker images
Docker images are found at https://hub.docker.com/r/ligaard/player/tags and can be run by `docker run -d -p 7778:7778 ligaard/player:fatjar-latest`.

## Starting from command line
Running from the command line: `$ ./gradlew player -Pcpf=http.cpf` (http.cpf is also the default). This will start the player service on localhost:7778.

There are two storage implementations to choose from: charlie.common.FakeCaveStorage and charlie.common.RedisCaveStorageConnector. For convenience a 'redis.cpf' is provided to use the redis connector to localhost:6379 by running `$ ./gradlew player -Pcpf=redis.cpf`. You can of course provide your own .cpf file, e.g. for running inside a docker compose network (see 'redis-compose.cpf').

## Docker Compose
Start a player container with a redis db (with volume) by running:

`$ docker stack deploy -c compose-player.yml <stack-name>`

This will start the services and be accessible from localhost:7778.

## Live running service
You can access the player service at `http://player.ligaard.dk:7778/skycave/v1/...`.

You can try a simple API call that should return 404 Not Found by clicking http://player.ligaard.dk:7778/skycave/v1/player/does-not-exist

## API specification
Specification format according to [FRDS book §7.7](https://leanpub.com/frds).
```
General info:

	All endpoints use JSON with Content-Type "application/json"

	positionString is using the following format: (x,y,z) example: (0,1,2)
	
	Unknown values in the JSON data will be ignored, and will not break the request.

--------------------------------- 
Get player by ID.

    GET /skycave/v1/player/{playerID}

    Response:
	Status: 200 OK
	{
		"playerID": "914f59dd9e15fe90237e3875",
		"playerName": "Will Crowther",
		"groupName": "Charlie-foxtrot",
		"positionAsString": "(3,2,1)",
		"region": "AARHUS",
		"accessToken": "6f9334b3-ced7-46ed-b4f8-002e49b15a42"
	}

	Status: 404 Not Found
	{}

404 is returned if the player is not found.

--------------------------------- 
Get players in room (computeListOfPlayersAt)

    GET /skycave/v1/players/{positionstring}

    Response:
	Status: 200 OK
	[
		{
			"playerID": "914f59dd9e15fe90237e3875",
			"playerName": "Will Crowther",
			"groupName": "Charlie-foxtrot",
			"positionAsString": "(3,2,1)",
			"region": "AARHUS",
			"accessToken": "6f9334b3-ced7-46ed-b4f8-002e49b15a42"
        	}, ...
	]

	Status: 400 Bad Request
	[]

200 OK: Will return a list of player(s) or empty list.

400 is returned if the positionString is malformed.

--------------------------------- 
Update player with ID (updatePlayerRecord)

    POST /skycave/v1/player/{id}
    {
        "playerID": "914f59dd9e15fe90237e3875",
        "playerName": "Will Crowther",
        "groupName": "Charlie-foxtrot",
        "positionAsString": "(3,2,1)",
        "region": "AARHUS",
        "accessToken": "6f9334b3-ced7-46ed-b4f8-002e49b15a42"
    }

    Response:
	Status: 200 OK
	Status: 201 Created
	Location: /skycave/v1/player/{id}
    {
        "playerID": "914f59dd9e15fe90237e3875",
        "playerName": "Will Crowther",
        "groupName": "Charlie-foxtrot",
        "positionAsString": "(3,2,1)",
        "region": "AARHUS",
        "accessToken": "6f9334b3-ced7-46ed-b4f8-002e49b15a42"
    }

	Status: 400 Bad Request

	Status: 404 Not Found

Calling with {id} will attempt to update an existing record. If successful response is 200 OK.

Calling without an {id} or a unknown {id} will attempt to create a new player record. If successful response is 201 Created.
```

### Health Check
For service monitoring the following API end-point exists:
```
Health Check

    GET /skycave/health

    Response:
        Status: 200 OK
        {
            "state": "healthy",
            "title": "PlayerService by Simon M. Møller and Kasper Ligaard",
            "version": "v1"
        }
```

## API Changes
Version 1.4 (from v1.3)
- Added the health check end-point

Version 1.3 (from v1.2):
- POST'ing with an unknown player ID will now create a player record with that ID. This change is needed to accomodate having an externalt subscription service managing player IDs. (Thanks to [Christian](https://bitbucket.org/ChristianMaintz/) for suggesting this).

Version 1.2 (from v1.1):
- Changed `positionString` to  `positionAsString`
- Changed region from integer to string. So instead of `3` you give e.g. `AARHUS`.